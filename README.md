# McPlarFailotic

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## MIT License 

Copyright (c) 2020

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Reason for MIT License 
Firstly, this is an open-source project. Using the MIT License allows me to share my code with a copyleft license without forcing anyone else to expose their own code. It allows my code to be very friendly for any business that wants to use it, because it lets my code be used for commercial purposes. The MIT license allows for the code to be freely distributed in a friendly manner and any users of this code are free to modify the original source code and distribute it as a separate project. Another reason for choosing the MIT License is that anyone who wants to use this code for private use is free to do so. There is no need for them to contribute back to the original project.

## Setup Instructions 

## Prequisites 

Before you attempt to clone and run this application, ensure that your development environment includes Node.js and an npm package manager.
You will also need a text editor such as Visual Studio Code.

## Step 1: Install the Angular CLI

Install the Angular CLI globally with this command: 
npm install -g @angular/cli

## Step 2: Clone this Repoistory 
git clone https://mattcorsetti@bitbucket.org/mattcorsetti/inf02300-assignment01.git

## Step 3: Cd to the location where you cloned the repository 

For example: 
cd C:\Users\username\code\INFO2300 Assignment01

## Step 4: Perform a npm install
Run the npm install command to ensure all the node modules get installed correctly

## Step 5: Run the application 

You can run the appication with this command: 
ng serve --open
